
if [ "$STOW_DIR" = "" ]
    printf "\033[32mWARNING Please set STOW_DIR for stow autocomplete to work"
end

function quickstow -d "Stows packages form $STOW_DIR/Cellar in $STOW_DIR"
    stow -v -t $STOW_DIR -d $STOW_DIR/Cellar $argv
end

function stowpkg-install -d "Runs mkdir build, cd build, cmake .. -DCMAKE_INSTALL_PREFIX=$STOW_DIR/Cellar/\$argv[1]"
    set package_name $argv[1]
    if [ "$package_name" = "" ]
        echo "ERROR Please provide a name for install destination (package_name = $package_name)"
        return 1
    end
    if [ "$STOW_DIR" = "" ]
        echo "ERROR Please set the STOW_DIR variable"
        return 1
    end
    mkdir -p build
    fish -c "cd build
    cmake .. -DCMAKE_INSTALL_PREFIX=$STOW_DIR/Cellar/$package_name
    make install"
end
complete -f -c stowpkg-install -a '(env -i ls $STOW_DIR/Cellar)'
complete -f -c quickstow -n 'contains -- -S (commandline -opc)' -a '(env -i ls $STOW_DIR/Cellar)'
complete -f -c quickstow -n 'contains -- -D (commandline -opc)' -a '(env -i ls $STOW_DIR/Cellar)'
complete -f -c quickstow -n 'contains -- -R (commandline -opc)' -a '(env -i ls $STOW_DIR/Cellar)'

complete -f -c stowpkg-clone -n 'contains -- --build-type (commandline -opc)' -a 'cmake make ninja none'

complete -f -c stowpkg-clone -a '-h' -d 'Print short help'
complete -f -c stowpkg-clone -a '--help' -d 'Print long help'
complete -f -c stowpkg-clone -a '--add-to-repos' -d 'Add to ~/.config/repos.yml for repos and rcd commands'
complete -f -c stowpkg-clone -a '--no-recursive' -d 'Do not put \'--recursive\' in \'git clone\' command'
complete -f -c stowpkg-clone -a '--repo' -d 'Repository URL'
complete -f -c stowpkg-clone -a '--build-type'
complete -f -c stowpkg-clone -a '--install-name' -d 'Name of package link in Cellar'
complete -f -c stowpkg-clone -a '--dry-run' -d 'Print commands that would be run without running them'
complete -f -c stowpkg-clone -a '--stow-dir' -d 'Specify stow directory (defaults to $STOW_DIR)'
complete -f -c stowpkg-clone -a '-t' -d 'Specify stow directory (Parameter of stow command) (defaults to $STOW_DIR)'
complete -f -c stowpkg-clone -a '--cellar-dir' -d 'Stow package directory (defaults to $STOW_DIR/Cellar)'
complete -f -c stowpkg-clone -a '-d' -d 'Stow package directory (Parameter of stow command) (defaults to $STOW_DIR/Cellar)'
complete -f -c stowpkg-clone -a '--source-dir' -d 'Main directory containing repos organized according to URL'
complete -f -c stowpkg-clone -a '--clone-location' -d 'Exact directory to clone repo in'
complete -f -c stowpkg-clone -a '--stow' -d 'Stow the package after build install and link'

