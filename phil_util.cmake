
macro(phil_default_install_prefix)
    if("${new_install_prefix}" STREQUAL "")
        set(new_install_prefix ${CMAKE_SOURCE_DIR}/localinstall)
    endif()

    if("${CMAKE_INSTALL_PREFIX}" STREQUAL "/usr/local")
        set(CMAKE_INSTALL_PREFIX ${new_install_prefix} CACHE FILEPATH "CMake Installation prefix for ${PROJECT_NAME}" FORCE)
        message(STATUS "(PHIL) Setting CMAKE_INSTALL_PREFIX to ${CMAKE_INSTALL_PREFIX}")
    endif()
endmacro()
