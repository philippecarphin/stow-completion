#!/usr/bin/env python3
import os
import argparse
import subprocess
import sys
import yaml
import shutil

class RepoError(Exception):
    pass

def url_to_directory(url):
    if url[0:4] == 'git@':
        return url[4:].replace(':', '/')
    elif url[:8] == 'https://':
        return url[8:]
    else:
        raise RepoError(f"URL '{url}' must begin with either 'git@' or 'https://'")

def get_args():
    p = argparse.ArgumentParser(description="Add a repo to my stow cellar")

    p.add_argument("--repo", help="URL of repo to clone", required=True)
    #p.add_argument("--command", help="Command to run in repo, ")
    p.add_argument("--build-type", help="Type of build", choices=['make', 'cmake', 'ninja', 'none', 'cargo'], required=True)
    p.add_argument("--install-name", help="Name of installed package inside Cellar")
    p.add_argument("--dry-run", action='store_true', help="Don't run commands")
    p.add_argument("--stow-dir", "-t", help="Stow directory (Defaults to value of 'STOW_DIR' environment variable)")
    p.add_argument("--cellar-dir", "-d", help="Cellar directory (Defaults to $STOW_DIR/Cellar environment variable)")
    p.add_argument("--source-dir", help="Directory containing source repositories (defaults to $HOME/Repositories")
    p.add_argument("--clone-location", help="Where to put the cloned repo, defaults to a subdirectory SOURCE_DIR formed from the repo URL")
    p.add_argument("--create-link", action='store_true', help="Create a link in CELLAR_DIR pointing to CLONE_LOCATION/localinstall")
    p.add_argument("--stow", help="Finish by stowing the package", action='store_true')
    p.add_argument("--add-to-repos", action='store_true', help="Add an entry in $HOME/config/repos.yml")
    p.add_argument("--no-recursive", action='store_true', help="Do not put '--recursive' in 'git clone' command")

    args = p.parse_args()

    if args.install_name is None:
        args.install_name = os.path.basename(args.repo)

    if args.stow_dir is None:
        if 'STOW_DIR' not in os.environ:
            raise RepoError(f"No value provided for '-t' and no environment variable STOW_DIR")
        stow_dir=os.environ['STOW_DIR']
        print(f'{sys.argv[0]}: \033[1;35mINFO\033[0m : Using environment variable STOW_DIR=\'{stow_dir}\' as Stow directory')
        args.stow_dir = stow_dir

    if args.cellar_dir is None:
        args.cellar_dir = os.path.join(args.stow_dir, 'Cellar')

    if args.clone_location:
        if args.source_dir is not None:
            print(f"Ignoring --source-dir in favor of --clone-location")
    else:
        if args.source_dir is None:
            try:
                home = os.environ['HOME']
            except KeyError:
                raise RepoError("No --source-dir specified.  Default relies on 'HOME' environment variable which does not exist")
            print(f"\033[32m[stowpkg-clone]: Using $HOME/Repositories/ as --source-dir\033[0m")
            args.source_dir = os.path.join(os.environ['HOME'], 'Repositories')

        args.clone_location = os.path.join(args.source_dir, url_to_directory(args.repo))

    return args

def main():

    r = Repo(origin=args.repo, location=args.clone_location)

    if os.path.exists(r.location):
        raise RepoError(f'Clone location \'{r.location}\' already exists')

    recursive = not args.no_recursive
    r.clone(recursive=recursive)
    if args.add_to_repos:
        r.add_to_repos()

    if args.build_type == 'make':
        r.run_command(f'make PREFIX={r.location}/localinstall install')
        r.run_command(f'ln -s {r.relative_location}/localinstall {args.cellar_dir}/{args.install_name}')
    elif args.build_type == 'cmake':
        r.run_command(f'mkdir build && cd build && cmake .. -DCMAKE_INSTALL_PREFIX=../localinstall && make install')
        r.run_command(f'ln -s {r.relative_location}/localinstall {args.cellar_dir}/{args.install_name}')
    elif args.build_type == 'cargo':
        r.run_command(f'cargo install --path . --root ./localinstall')
        r.run_command(f'ln -s {r.relative_location}/localinstall {args.cellar_dir}/{args.install_name}')

    if args.stow and args.build_type != 'none':
        r.run_command(f'stow -v -t {args.stow_dir} -d {args.cellar_dir} -R {args.install_name}')

class Repo():
    def __init__(self, origin, location):
        self.origin = origin
        self.name = os.path.basename(origin)
        self.location = location
        self.container_location = os.path.dirname(location)
        self.relative_location = os.path.relpath(self.location, start=args.cellar_dir)

    def __str__(self):
        return f'Repo : origin = {self.origin}, location = {self.location}'

    def fetch_src(self):
        self.run_command(f'git fetch -p origin')

    def push_dst(self):
        self.run_command(f'git push --mirror')

    def setup_push_remote(self):
        self.run_command(f'git remote set-url --push origin {self.location}')

    def clone(self, recursive=True):
        os.makedirs(self.container_location, exist_ok=True)
        rec = '--recursive' if recursive else ''
        result = self.run_container_command(f'git clone {rec} {self.origin} {self.location}')
        if not args.dry_run and result.returncode != 0:
            shutil.rmtree(self.location)
            print(f"Clone unsuccessful.  Deleted clone {self.location} to allow retry")
            raise RepoError(f"Failed to clone repository: clone command returned {result.returncode}")


    def run_command(self, cmd):
        if args.dry_run:
            print(f'\033[33m[stowpkg-clone:{self.name}] (dry-run)\033[0m {cmd}')
            return

        print(f'\033[32m[stowpkg-clone:{self.name}] {cmd}\033[0m')
        subprocess.run(f'cd {self.location} && {cmd}', shell=True, check=True)

    def run_container_command(self, cmd):
        if args.dry_run:
            print(f'\033[33m[stowpkg-clone:{self.name}] (dry-run)\033[0m cd {self.container_location} && {cmd}')
            return

        print(f'\033[32m[stowpkg-clone:{self.name}] {cmd}\033[0m')
        return subprocess.run(f'cd {self.container_location} && {cmd}', shell=True, check=True)

    def add_to_repos(self):
        print(f'\033[32m[stowpkg-clone:{self.name}] Adding entry \'{self.name}\' to ~/.config/repos.yml\033[0m')
        repo_file = os.path.join(os.environ['HOME'], '.config', 'repos.yml')
        with open(repo_file) as y:
            d = yaml.safe_load(y)

        d['repos'][self.name] = {"path": self.location}

        with open(repo_file, 'w') as y:
            yaml.dump(d,y)


if __name__ == "__main__":
    errors = 0
    try:
        args = get_args()
        main()
    except subprocess.CalledProcessError as e:
        print(f'{os.path.basename(sys.argv[0])}: \033[1;31mERROR\033[0m: Command "{e.cmd}" failed : see above for details')
        errors += 1
    except RepoError as e:
        print(f'{os.path.basename(sys.argv[0])}: \033[1;31mERROR\033[0m: {e}')
        errors += 1

    sys.exit(errors)
