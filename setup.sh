#!/usr/bin/env -S bash -o errexit -o nounset -o errtrace -o pipefail -O inherit_errexit -O nullglob -O extglob

true_home=$(cd -P $HOME && pwd)
this_dir=$(cd -P $(dirname $0) && pwd)
this_dir_rel_home=${this_dir##${true_home}/}

cmake -S ${this_dir} -B ${this_dir}/build -DBUILD_STOW=TRUE -DCMAKE_INSTALL_PREFIX=${this_dir}/localinstall
cmake --build ${this_dir}/build
cmake --install ${this_dir}/build
mkdir -p $HOME/fs1/Cellar
ln -sf ../../${this_dir_rel_home}/localinstall $HOME/fs1/Cellar/stow-completion-latest
PATH=${this_dir}/localinstall/bin:$PATH quickstow -S stow-completion-latest
